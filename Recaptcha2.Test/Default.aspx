﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Recaptcha2.Test.Default" %>

<%@ Register Assembly="Recaptcha2.Web" Namespace="Recaptcha2.Web.UI.Controls" TagPrefix="captcha" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <captcha:Recaptcha2 ID="Recaptcha1"  Theme="dark light" ForeColor="Red" runat="server" ErrorMessage="" />
        
        <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" UseSubmitBehavior="false"/>
    </form>
</body>
</html>
