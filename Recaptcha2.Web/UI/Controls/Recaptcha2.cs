﻿using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Recaptcha2.Web.UI.Controls
{
	[ToolboxData("<{0}:Recaptcha2 runat=\"server\"></{0}:Recaptcha2>"), DefaultProperty("PublicKey")]
    public class Recaptcha2 : WebControl, IValidator
    {
		private Label _label;
		private RecaptchaVerificationHelper2 _verifier;

		protected override void OnInit(System.EventArgs e)
		{
			base.OnInit(e);
			_verifier = new RecaptchaVerificationHelper2(PrivateKey);
			Page.Validators.Add(this);
		}

		protected override void OnUnload(System.EventArgs e)
		{
			if (Page != null)
			{
				Page.Validators.Remove(this);
			}
			base.OnUnload(e);
		}

		protected override void CreateChildControls()
		{
			_label = new Label();
			_label.Visible = false;
			_label.Font.Bold = true;

			Controls.Add(_label);
		}

		protected override void RenderContents(HtmlTextWriter output)
		{
			if (DesignMode)
			{
				output.Write("<p>Recaptcha Control</p>");
			}
			else
			{
				output.BeginRender();

				output.RenderBeginTag(HtmlTextWriterTag.Table);
				
				output.RenderBeginTag(HtmlTextWriterTag.Tr);
				output.RenderBeginTag(HtmlTextWriterTag.Td);
				output.Write(new RecaptchaHtmlHelper2(PublicKey, Theme, Language, TabIndex).ToString());
				output.RenderEndTag();
				output.RenderEndTag();

				if (!string.IsNullOrWhiteSpace(ErrorMessage))
				{
					output.RenderBeginTag(HtmlTextWriterTag.Tr);
					output.RenderBeginTag(HtmlTextWriterTag.Td);
					_label.Visible = true;
					_label.Text = ErrorMessage;
					_label.RenderControl(output);
					output.RenderEndTag();
					output.RenderEndTag();
				}

				output.RenderEndTag();

				output.EndRender();
			}
		}

		[Category("Appearance"), Localizable(false), Bindable(true)]
		public Color ForeCollor
		{
			get
			{
				return _label.ForeColor;
			}
			set
			{
				_label.ForeColor = value;
			}
		}

		[Category("Appearance"), Localizable(false), Bindable(true), DefaultValue("en")]
		public string Language
		{
			get
			{
				string result = ViewState["RecaptchaLanguage"] as string;

				if (string.IsNullOrWhiteSpace(result))
				{
					result = "en";
				}

				return result;
			}
			set
			{
				ViewState["RecaptchaLanguage"] = value;
			}
		}

		[DefaultValue("{recaptchaPrivateKey}"), Category("Behavior"), Bindable(true), Localizable(false)]
		public string PrivateKey
		{
			get
			{
				string result = ViewState["PrivateKey"] as string;

				if (string.IsNullOrWhiteSpace(result))
				{
					result = "{recaptchaPrivateKey}";
				}
				return result;
			}
			set
			{
				ViewState["PrivateKey"] = value;
			}
		}

		[Category("Behavior"), DefaultValue("{recaptchaPublicKey}"), Localizable(false), Bindable(true)]
		public string PublicKey
		{
			get
			{
				string result = ViewState["PublicKey"] as string;

				if (string.IsNullOrWhiteSpace(result))
				{
					result = "{recaptchaPublicKey}";
				}
				return result;
			}
			set
			{
				ViewState["PublicKey"] = value;
			}
		}

		[Localizable(false), Bindable(true), Category("Appearance"), DefaultValue("light")]
		public string Theme
		{
			get
			{
				string result = ViewState["RecaptchaTheme"] as string;

				if (string.IsNullOrWhiteSpace(result))
				{
					result = "light";
				}

				return result;
			}
			set
			{
				ViewState["RecaptchaTheme"] = value;
			}
		}

		public string ErrorMessage
		{
			get
			{
				return _verifier.ErrorMessage;
			}
			set { }
		}

		public bool IsValid
		{
			get
			{
				return _verifier.IsValid;
			}
			set { }
		}

		public void Validate()
		{
			_verifier.Validate();
		}
	}
}
