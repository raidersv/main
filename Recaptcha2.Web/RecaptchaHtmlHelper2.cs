﻿using System;
using System.Text;
using System.Threading;
using System.Web;

namespace Recaptcha2.Web
{
	public class RecaptchaHtmlHelper2
	{
		public RecaptchaHtmlHelper2(string publicKey)
		{
			if (string.IsNullOrEmpty(publicKey))
			{
				throw new InvalidOperationException("Public key cannot be null or empty.");
			}

			PublicKey = RecaptchaKeyHelper.ParseKey(publicKey);
			UseSsl = HttpContext.Current.Request.Url.AbsoluteUri.StartsWith("https");
		}

		public RecaptchaHtmlHelper2(string publicKey, string theme, string language, int tabIndex) : this(publicKey)
		{
			Theme = theme;
			Language = language;
			TabIndex = tabIndex;
		}

		public string Language { get; set; }

		public string PublicKey { get; private set; }

		public int TabIndex { get; set; }

		public string Theme { get; set; }

		public bool UseSsl { get; private set; }

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();

			string language = Language;
			if (string.IsNullOrWhiteSpace(language))
			{
				language =  Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
			}

			sb.AppendFormat("<script src='http{0}://www.google.com/recaptcha/api.js?hl={1}'></script>", 
				UseSsl ? "s" : string.Empty, language);

			sb.AppendFormat("<div class='g-recaptcha' data-sitekey='{0}' data-theme='{1}'></div>", 
				PublicKey, Theme);

			return sb.ToString();
		}
	}
}
