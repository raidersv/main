﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Recaptcha2.Web
{
	[DataContract]
	public class RecaptchaApiResponse
	{
		[DataMember(Name = "success")]
		public bool Success;

		[DataMember(Name = "error-codes")]
		public List<string> ErrorCodes;
	}
}
