﻿using System;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;

namespace Recaptcha2.Web
{
	public class RecaptchaVerificationHelper2
	{
        private RecaptchaVerificationHelper2()
        {
			ErrorMessage = string.Empty;
        }

		internal RecaptchaVerificationHelper2(string privateKey) : this()
        {
            if (string.IsNullOrEmpty(privateKey))
            {
                throw new InvalidOperationException("Private key cannot be null or empty.");
            }
            if ((HttpContext.Current == null) || (HttpContext.Current.Request == null))
            {
                throw new InvalidOperationException("Http request context does not exist.");
            }

            HttpRequest request = HttpContext.Current.Request;
            PrivateKey = privateKey;
            UserHostAddress = request.UserHostAddress;
			Response = request["g-recaptcha-response"];

			if (!string.IsNullOrEmpty(Response))
            {
				Response = request.Params["g-recaptcha-response"];
            }
        }

		private string PrivateKey { get; set; }

		private string UserHostAddress { get; set; }

		public string Response { get; private set; }

		public string ErrorMessage { get; private set; }

		public bool IsValid
		{
			get { return string.IsNullOrWhiteSpace(ErrorMessage); }
		}

		public void Validate()
		{
			if (string.IsNullOrEmpty(Response))
			{
				ErrorMessage = "Captcha is not entered";
				return;
			}

			ErrorMessage = string.Empty;

			string url = string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}&remoteip={2}",
				RecaptchaKeyHelper.ParseKey(PrivateKey), Response, UserHostAddress);

			try
			{
				WebRequest request = WebRequest.Create(url);
				request.Method = "GET";
				IWebProxy systemWebProxy = WebRequest.GetSystemWebProxy();
				systemWebProxy.Credentials = CredentialCache.DefaultCredentials;
				request.Proxy = systemWebProxy;

				using (WebResponse webResponse = request.GetResponse())
				{
					var serializer = new DataContractJsonSerializer(typeof(RecaptchaApiResponse));
					RecaptchaApiResponse response = serializer.ReadObject(webResponse.GetResponseStream()) as RecaptchaApiResponse;

					if (!response.Success && response.ErrorCodes != null)
					{
						StringBuilder sb = new StringBuilder();
						foreach (string errorCode in response.ErrorCodes)
						{
							switch (errorCode)
							{
								case "missing-input-secret":
									if (sb.Length > 0) sb.Append("<br/>");
									sb.Append("The secret parameter is missing.");
									break;
								case "invalid-input-secret":
									if (sb.Length > 0) sb.Append("<br/>");
									sb.Append("The secret parameter is invalid or malformed.");
									break;
								case "missing-input-response":
									if (sb.Length > 0) sb.Append("<br/>");
									sb.Append("The response parameter is missing.");
									break;
								case "invalid-input-response":
									if (sb.Length > 0) sb.Append("<br/>");
									sb.Append("The response parameter is invalid or malformed.");
									break;
							}
						}

						if (sb.Length > 0) ErrorMessage = sb.ToString();
					}
				}
			}
			catch (Exception exc)
			{
				ErrorMessage = exc.Message;
				throw exc;
			}
		}
	}
}
